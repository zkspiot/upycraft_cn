## 拍照

### 准备
硬件：
* FireBeetle-ESP32 × 1
* FireBeetle Covers-Camera&Audio Media Board模块 × 1 （<a href="http://wiki.dfrobot.com.cn/index.php?title=(SKU:DFR0498)FireBeetle_Covers-Camera%26Audio_Media_Board" title="0498" target="_blank">点击查看</a>模块详情）
* OV7725摄像头 × 1
* SD卡 × 1

软件：
* uPyCraft IDE
* camera.exe  （<a href="https://gitee.com/dfrobot/upycraft/raw/master/camera.exe" title="0498" target="_blank">点击下载</a> 该软件）

代码位置：
* File → Examples → Camera → <b>camera.py</b>

### 实验步骤
<p>1. 将摄像头和SD卡分别插入模块对应的位置，如下图</p>

<img src=images/5.64.PNG>

<p>2. 修改 camera.py 文件中的WiFi名称和密码，并下载运行，具体代码如下</p>

**注意：**
<br>&nbsp;&nbsp;&nbsp;电脑和WiFi应处在同一网段，以保证能够达到预期的实验效果。

```py
#硬件平台：FireBeetle-ESP32

from machine import IIS
from machine import Pin
import network
import time

SSID = "XXXXXXXX"                     #设置wifi账号
PASSWORD = "XXXXXXXX"                 #设置wifi密码
wlan = None
camera = IIS(IIS.CAMERA)              #创建一个iis对象并设置模式
button = Pin(16, Pin.IN)

def connectWifi(ssid, passwd):
  global wlan
  wlan = network.WLAN(network.STA_IF) #创建一个wlan对象
  wlan.active(True)                   #激活网络接口
  wlan.disconnect()                   #断开最后连接的WiFi
  wlan.connect(ssid, passwd)          #连接无线网络
  while(wlan.ifconfig()[0] == '0.0.0.0'):
    time.sleep(1) 
connectWifi(SSID, PASSWORD)   

camera.init()                         #初始化摄像头
camera.setFramesize(IIS.HQVGA)        #设置画面分辨率
camera.setPixformat(IIS. GRAYSCALE)   #设置照片格式
camera.httpServerStart()              #打开http服务器

#捕获异常，如果意外中断，停止照相
try:
  while True:
    pass
except:
  camera.httpServerStop() 
  wlan.disconnect()
```
代码说明：分辨率只能设置为IIS.HQVGA
<p>3. 运行后将终端中的ip地址填入camera.exe中，并点击Start按钮，如下图</p>

<img src=images/5.17.png>

### 实验效果
<img src=images/5.18.2.png>

<h1><b>附:</b></h1>

<br>FireBeetle Covers-Camera&Audio Media Board模块与ESP32主板控制器引脚连接对应关系图。

<img src=images/5.63.png>