&nbsp;&nbsp;&nbsp;&nbsp;双击uPyCraft.exe即可进入MicroPython的IDE环境，如下图所示。当检测到当前版本低于最新版本（如当前版本为0.25，最新版本为0.26），IDE会提示升级，点击“OK”后下载新版本，下载完成后会提示删除旧版本。

<img src=images/4.8.1.png>

<p id = "color"style="color:red;"><b>目录树</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;在整个界面的左侧，可以通过不同的文件目录来管理文件，包括目录device，sd，uPy_lib，workSpace等。其中</p>

* device：显示已连接上的开发板上存在的文件。
* sd：目前版本尚未支持。
* uPy_lib：显示IDE自带的库文件。
* workSpace：用户自定义目录，保存用户自己的文件。

<p id = "color"style="color:red;"><b>编辑窗口</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;用户在这个区域中可以编辑修改文件，一般源程序的编辑及修改都在这个窗口完成。编辑窗口顶部是文件标签，显示你打开了哪些文件，将鼠标停留在文件名上可以查看它的保存位置。在编辑窗口点击鼠标右键可对文件内容进行复制，粘贴等操作。</p>
<p id = "color"style="color:red;"><b>终端框</b>
<p>&nbsp;&nbsp;&nbsp;&nbsp;在界面的下方，用于命令行的执行，显示程序执行的信息，显示提示信息，如果有错误则显示错误信息等。
<br>&nbsp;&nbsp;&nbsp;&nbsp;鼠标右键即可对终端框中的选中内容进行复制和粘贴操作，如下图。

<img src=images/4.26.png>

<p id = "color"style="color:red;"><b>菜单栏</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;在界面的上方，几乎包含uPyCraft的所有操作。</p>

<p id = "color"style="color:red;"><b>工具栏</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;在菜单栏的下方，提供基本的快捷操作，具体的介绍如下图。</p>
    
<img src=images/4.7.png>

工具栏从左到右依次是：
<br>1：New（新建）
<br>2：Open（打开）
<br>3：Save（保存）
<br>4：DownloadAndRun（下载并运行）
<br>5：Stop（停止运行）
<br>6：connect/disconnect（连接或断开USB串口） 
<br>7：Undo（撤销）
<br>8：Redo（恢复） 
<br>9：SyntaxCheck（语法检测）
<br>10：Clear（清空终端）