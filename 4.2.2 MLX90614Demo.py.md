## MLX90614测量温度

### 准备
硬件：
* FireBeetle-EPS32 × 1
* IR Thermometer Sensor-MLX90614 红外温度传感器 × 1（<a href="http://wiki.dfrobot.com.cn/index.php?title=(SKU:SEN0206)IR_Thermometer_Sensor-MLX90614_%E7%BA%A2%E5%A4%96%E6%B8%A9%E5%BA%A6%E4%BC%A0%E6%84%9F%E5%99%A8" title="1602" target="_blank">点击查看</a>模块详情）
<a href="http://www.dfrobot.com.cn/goods-1302.html" title="1602" target="_blank"><h4>点击购买</h4></a>

软件：
* uPyCraft IDE

代码位置：
* File → Examples → Measure → <b>MLX90614Demo.py</b>
* 引用模块：uPy_lib → <b>MLX90614.py</b>

### 实验步骤
<p>1. 将红外温度传感器模块与ESP32相连接，如下图</p>
<img src=images/5.58.png>
<p>2. 将 MLX90614.py 文件拖动到device目录下，如下图</p>
<img src=images/5.44.png>
<p>3. 下载运行 MLX90614Demo.py 文件，具体代码如下</p>

```py
#硬件平台：FireBeetle-ESP32
#实验效果：这个实验用来检测环境温度。
#硬件连接：本实验需要外接一个'MLX90614'红外温度传感器，通过I2C通讯，IO22(SCL)、IO21(SDA)。
#下面的信息显示，对于当前版本，MLX90614Demo是可用的。
# IO0  IO2  IO4  IO5  IO9  IO10  IO21~23  IO25~27

import MLX90614
from machine import Pin, I2C
import time

i2c = I2C(scl=Pin(22), sda=Pin(21), freq=100000)    
ir = MLX90614.MLX90614(i2c)                         

while True:
  time.sleep(1)
  print("Object  %s *C"% ir.getObjCelsius())     #获取物体的摄氏温度
  print("Object  %s *F"% ir.getObjFahrenheit())  #获取对象的华氏温度
  print("Ambient %s *C"% ir.getEnvCelsius())     #获取环境的摄氏温度
  print("Ambient %s *F"% ir.getEnvFahrenheit())  #获取环境的华氏温度
  print()
```

### 实验效果
<img src=images/5.45.png>

### MLX90614.py库API说明

#### **类 — MLX90614**
* class MLX90614(i2c, addr=MLX90614_IIC_ADDR)

```
i2c：创建的i2c对象
```

#### **类函数**
* getObjCelsius()
函数功能：获取物体的摄氏温度。
* getEnvCelsius()
函数功能：获取环境的摄氏温度。
* getObjFahrenheit()
函数功能：获取物体的华氏温度。
* getEnvFahrenheit()
函数功能：获取环境的华氏温度。
* getTemp(reg)
函数功能：温度转换

```
reg：传感器的寄存器地址
```
* getReg(reg)
函数功能：接收传感器数据。

```
reg：传感器的寄存器地址
```