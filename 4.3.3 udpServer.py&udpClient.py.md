## 基于UDP协议的通信

### 准备
硬件：
* FireBeetle-ESP32 × 1

软件：
* uPyCraft IDE
* 串口调试助手  <p>下载地址：git.oschina.net/dfrobot/upycraft/raw/master/sscom5.12.1.exe</p>

代码位置：
* File → Examples → Communicate → <b>udpServer.py</b>
* File → Examples → Communicate → <b>udpClient.py</b>

### 实验步骤

#### **UDPServer**
<p>1. 修改udpServer.py中的WiFi名称和密码，并下载运行，如下图</p> 

<img src=images/5.28.png>

<p>2. 打开SSCOM，端口号选择UDP，将SSCOM远程IP地址修改为IDE终端中打印的IP地址，端口号与udpServer.py中一致，完成后点击连接，如下图</p>

<img src=images/5.29.png>

<p>3. 在SSCOM输入框中输入“hello”，可以在IDE中看到客户端发送的信息</p>

#### 实验效果
<img src=images/5.30.png>

#### **UDPClient**

<p>1. 按照如下图修改 udpClient.py，其中SSID和PASSWORD为你的WiFi名称和密码，SSCOM中的本地IP地址为你电脑的IP地址。
<img src=images/5.31.png>

<p>2. 下载运行udpClient.py 文件，将运行后终端中出现的IP地址复制到SSCOM中远程IP地址框中，如下图</p>
<img src=images/5.32.png>

### 实验效果
<p>点击SSCOM的连接，即可看到客户端发来的信息，如下图</p>
<img src=images/5.33.png>