## 读取模拟口

### 准备
硬件：
* FireBeetle-ESP32 × 1

软件：
* uPyCraft IDE

代码位置：
* <p>File → Examples → Basic → <b>analogRead.py</b></p>

### 实验步骤
<p>下载运行 analogRead.py，其代码如下</p>

```py
#硬件平台：FireBeetle-ESP32
#实验效果：读取IO口的模拟电压。
#下面的信息显示，对于当前版本，analogRead是可用的。
# A0/IO36   A1/IO39   A2/IO34   A3/IO35   A4/IO15
#FireBeetle-ESP32 一共有五个模拟输入口，即A0~A4。)

from machine import ADC, Pin
import time

adc0 = ADC(Pin(36))  #A0/IO36
#adc1 = ADC(Pin(39))
#adc2 = ADC(Pin(34))
#adc3 = ADC(Pin(35))
#adc4 = ADC(Pin(15))

while True:
  print("adc0=", adc0.read())  #读取IO36引脚模拟量进行模数转化后的数字量
  #print("adc1=", adc1.read())
  #print("adc2=", adc2.read())
  #print("adc3=", adc3.read())
  #print("adc4=", adc4.read())
  
  time.sleep(0.1)
```

### 实验效果
&nbsp;&nbsp;&nbsp;不断读取ADC的值并打印到终端。

### 实验扩展
&nbsp;&nbsp;&nbsp;将下面代码保存为 .py 文件并运行，你将看到LED灯亮度随输入电压大小变化而改变。

```py
from machine import ADC, Pin, PWM
import time

adc = ADC(Pin(39))             #设置IO39为模拟IO口
adc.atten(ADC.ATTN_11DB)       #满量程：3.3v
adc.width(ADC.WIDTH_12BIT)     #设置12位数据宽度

pwm = PWM(Pin(2), 100)         #创建PWM对象
pwm.duty(50)
while True:
  dc = adc.read()              #读取IO口模拟电压值
  dc = dc * 3.3 / 4095         #转换成电压值 0~3.3V
  print(dc,"V")
  duty = int(dc * 1023 // 3.3) #计算占空比
  pwm.duty(duty)               #设置2引脚输出的PWM的占空比（控制led灯的亮度）
  time.sleep(0.1)
```