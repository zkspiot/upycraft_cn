## ADC
**注意**：ADC在专用引脚上可用，可用引脚有：A2/IO34、A3/IO35、A0/IO36、A1/IO39，可测电压范围为0~3.3V。

### 类

#### class machine.ADC(pin) 

	pin：I/O引脚
    	Pin(34)、Pin(35)、Pin(36)、Pin(39)

#### 定义ADC对象
示例：

```py
from machine import ADC, Pin

adc = ADC(Pin(34))
```

### 函数

#### 1. ADC.read()
函数说明：读取ADC并返回读取结果。
<br>示例：

```py
x = adc.read()
print(x)
```

#### 2. ADC.atten(db)
函数说明：设置衰减比（即满量程的电压，比如11db满量程时电压为3.3V）。
	
	db：衰减比
    	ADC.ATTIN_0DB、ADC.ATTN_2_5_DB、ADC.ATTN_6DB、ADC.ATTN_11DB
示例：

```py
adc.atten(ADC.ATTN_0DB)
```

#### 3. ADC.width(bit)
函数说明：设置数据宽度。

	bit：
    	ADC.WIDTH_9BIT、 ADC.WIDTH_10BIT、ADC.WIDTH_11BIT、 ADC.WIDTH_12BIT

### 宏
衰减比
* ADC.ATTN_0DB    &nbsp;&nbsp;= 0&nbsp;&nbsp;&nbsp; — 满量程：1.2v
* ADC.ATTN_2_5DB   &nbsp;&nbsp;= 1&nbsp;&nbsp;&nbsp; — 满量程：1.5v
* ADC.ATTN_6DB   &nbsp;&nbsp;= 2&nbsp;&nbsp;&nbsp; — 满量程：2.0v
* ADC.ATTN_11DB   &nbsp;&nbsp;= 3&nbsp;&nbsp;&nbsp; — 满量程：3.3v

数据宽度
* ADC.WIDTH_9BIT   &nbsp;&nbsp;= 0&nbsp;&nbsp;&nbsp; — 9位数据宽度， 即满量程0x1ff(511) 
* ADC.WIDTH_10BIT  &nbsp;&nbsp;= 1&nbsp;&nbsp;&nbsp; — 10位数据宽度，即满量程0x3ff(1023)
* ADC.WIDTH_11BIT  &nbsp;&nbsp;= 2&nbsp;&nbsp;&nbsp; — 11位数据宽度，即满量程0x7ff(2047)
* ADC.WIDTH_12BIT  &nbsp;&nbsp;= 3&nbsp;&nbsp;&nbsp; — 12位数据宽度，即满量程0xfff(4095)

### 综合示例

```py
from machine import ADC, Pin
import time

adc = ADC(Pin(36))            #设置IO36为模拟IO口
adc.atten(ADC.ATTN_11DB)      #设置衰减比
adc.width(ADC.WIDTH_12BIT)    #设置12位数据宽度
for i in range(0, 10):
  print("adc0 =", adc.read()) #读取IO口模拟电压值
  time.sleep(0.5)
```

## DAC
**注意**：DAC在专用引脚上可用，可用引脚有：IO25/D2、IO26/D3，电压范围为0~3.3V。

### 类

#### class machine.DAC(pin)

	pin：I/O引脚
    	Pin(25)、Pin(26)

#### 定义DAC
示例：

```py
from machine import DAC, Pin

dac = DAC(Pin(26))
```

#### 函数

#### DAC.write(value)
函数说明：设置输出电压（0~3.3V）。

	value：
    	0 ≤ value ≤ 255（对应0~3.3V）
示例：

```py
dac.write(255)
```

#### 综合示例

```py
from machine import DAC, Pin

dac = DAC(Pin(25))  #创建一个DAC对象
dac.write(245)      #输出电压
```