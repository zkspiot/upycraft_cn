&nbsp;&nbsp;&nbsp;sys模块中提供了与MicroPython运行环境有关的函数和变量。

* ####  sys.path — 系统路径
<br>示例：

```py
>>> import sys
>>> print(sys.path)
['', '/lib']
```

* #### sys.version — MicroPython 语言版本
<br>示例：

```py
>>> print(sys.version)
3.4.0
```

* ####  sys.implementation — 当前运行环境
<br>示例：

```py
>>> print(sys.implementation)
(name='micropython', version=(1, 9, 1))
```
&nbsp;&nbsp;&nbsp;对于 MicroPython，它返回下面属性:

	名称 - “micropython”
	版本 - (主, 次, 微), 如 (1, 7, 0)

&nbsp;&nbsp;&nbsp;这个方法推荐用来识别不同平台的MicroPython。

* #### sys.platform — 获取MicroPython运行的平台
<br>示例：

```py
>>> sys.platform
'esp32'
```
* #### sys.byteorder — 字节顺序 （“小”或“大”）
<br>示例：

```py
>>> print(sys.byteorder)
little
```
“little”表示小端存储，“big”表示大端存储。

* #### sys.maxsize — 整数类型最大的数值。
<br>示例：

```py
>>> print(sys.maxsize)
2147483647
```
* #### sys.stdin — 标准输入设备。

* #### sys.stdout — 标准输出设备。
 
* #### sys.stderr — 标准错误输出设备。

* #### sys.modules — 已载入模块字典。在某些移植版中，它可能不包含在内建模块中。


### 函数
&nbsp;&nbsp;&nbsp;sys模块中提供了绑定在系统路径上的函数，通过help()可以查看，这里不作介绍。
<br>示例：

```py
>>> help(sys.path)
object ['', '/lib'] is of type list
  append -- <function>
  clear -- <function>
  copy -- <function>
  count -- <function>
  extend -- <function>
  index -- <function>
  insert -- <function>
  pop -- <function>
  remove -- <function>
  reverse -- <function>
  sort -- <function>
>>> sys.path.append('test')
>>> sys.path
['', '/lib', 'test']
```

#### sys.exit()
函数说明：退出当前程序。
<br>示例：

```py
>>> i = 0
>>> while True:
...   i = i+1
...   print(i)
...   if(i == 6):
...     sys.exit()
...     
...     
... 
1
2
3
4
5
6
```