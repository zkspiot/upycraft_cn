## 波形输出

### 准备
硬件：
* FireBeetle-ESP32 × 1 
* 示波器 × 1 （查看波形） 

软件：
* uPyCraft IDE

代码位置：
* <p>File → Examples → Basic → <b>waveform.py</b></p>

### 实验步骤
<p>1. 硬件连接：将IO25/D2引脚和IO26/D3引脚分别接上示波器。</p>
<p>2. 下载运行 waveform.py，具体代码如下</p>

```py
#硬件平台：FireBeetle-ESP32
#实验效果：IO25输出正弦波，IO26输出锯齿波。
#硬件连接：如果需要观察波形，需要在IO25和IO26接上示波器。
#下面的信息显示，对于当前版本，waveform是可用的。
# IO25  IO26
#FireBeetle-ESP32 一共有两个DAC信号引脚。

from machine import DAC,Pin
import math
import time

dac0 = DAC(Pin(25))
dac1 = DAC(Pin(26))

a = 0
while True:
  value = math.sin(a*math.pi/180)        
  dac0.write(int(100+value*100))  #输出正弦波     
  dac1.write(a*255//360)          
  a += 1
  if(a == 361):
    a=0
  time.sleep(0.0001)
```

### 实验效果

#### 正弦波：

<img src=images/4.1.png>
            
#### 锯齿波：
<img src=images/4.2.png>
  