### 函数

#### 1. esp.flash_read(byte_offset, buf)
函数说明：从地址为 byte_offset 的 flash 起始点读取 buf.len()个长度的数据并放入 buf 中。

	byte_offset：flash偏移地址
    buf：接收数据缓冲区，缓冲区的长度为len
示例：

```py
import esp

buf = bytearray(100)
esp.flash_read(2097152, buf)
```

#### 2. esp.flash_write(byte_offset, buf)
函数说明：将 buf 中所有的数据写入地址为 byte_offset 的 flash 区域。

	byte_offset：flash偏移地址
    buf：数据缓冲区，缓冲区长度为len
示例：

```py
buf = bytearray(100)
esp.flash_write(2097152, buf)
```

#### 3.  esp.flash_erase(sector_no)
函数说明：擦除flash扇区。

	sector_no：要擦除的扇区
示例：

```py
esp.flash_erase(512)
```

#### 4.  esp.flash_size()
函数说明：返回flash的大小。

#### 5.  esp.flash_user_start()
函数说明：返回用户可使用flash起始地址。

#### 6.  esp.neopixel_write(pin, grb_buf, timing)
函数说明：该函数可用于控制LED灯条。

	pin：LED灯条连接的引脚
    grb_buf：rgb颜色
    timing：频率
示例：

```py
from machine import Pin
import esp

pin = Pin(2, Pin.OUT)
rgb = bytearray([255, 0, 0])  #红色
while True:
  esp.neopixel_write(pin, rgb, 800)
```

#### 7.  esp.dht_readinto(pin, buf)
函数说明：读dht的值（温湿度传感器）。

	pin：读取数据的引脚
    buf：数据缓冲区
示例：
<br>&nbsp;&nbsp;&nbsp;本示例需要连接DHT11模块，<a href="http://www.dfrobot.com.cn/goods-109.html" title="DHT11" target="_blank">点击购买</a>DHT11

```py
from machine import Pin
import time
import esp
buf = bytearray(5)
    
def measure(pin):
  global buf
  esp.dht_readinto(pin, buf)
  if(buf[0]+buf[1]+buf[2]+buf[3])&0xff!= buf[4]:
    raise Exception("checksum error")

def dht11_humidity():
  return buf[0]
  
def dht11_temperature():
  return buf[2]
    
def dht22_humidity():
  return (buf[0]<<8 | buf[1])*0.1
    
def dht22_temperature():
  t = ((buf[2] & 0x7f) << 8 |buf[3])*0.1
  if buf[2] & 0x80:
    t = -t
  return t

try:
  while True:
    measure(Pin(2))
    print("dht11 humidity:", dht11_humidity())
    print("dht11 temperature:", dht11_temperature())
   # print("dht22 humidity:", dht22.humidity())
   # print("dht22 temperature:", dht22.temperature())
    time.sleep(0.5)

except:
  print("Abnormal program!")
```
#### 运行结果

```py
dht11 humidity：18
dht11 temperature：24
dht11 humidity：18
dht11 temperature：24
dht11 humidity：18
dht11 temperature：24
……
```