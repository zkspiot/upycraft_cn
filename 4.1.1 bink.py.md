## 点亮LED灯

### 准备
硬件：
* FireBeetle-ESP32 × 1

软件：
* uPyCraft IDE

代码位置：
* <p>File → Examples → Basic → <b>blink.py</b></p>

### 实验步骤
<p>下载运行  blink.py，其代码如下</p>

```py
#硬件平台: FireBeetle-ESP32
#实验效果：呈现闪烁效果。
#下面的信息显示，对于当前版本，blink是可用的。
# IO0  IO4  IO10 IO12~19  IO21~23 IO25~27
#IO2引脚与板载LED相连，不需要外接LED灯，其他引脚需要外接LED灯。

import time
from machine import Pin

led=Pin(2, Pin.OUT) #将led引脚定义为输出

while True:
  led.value(1)      #点亮led
  time.sleep(0.5)
  led.value(0)      #熄灭led
  time.sleep(0.5)
```

### 实验效果
&nbsp;&nbsp;&nbsp;led灯每隔0.5秒闪烁。