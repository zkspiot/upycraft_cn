&nbsp;&nbsp;&nbsp;TouchPad在专用引脚上可用，可用引脚有：IO0、IO2/D9、D0/IO4、MCLK/IO12、IO13/D7、BCLK/IO14、A4/IO15、IO27/D4。

### 类

#### class machine.TouchPad(pin)

	pin：引脚
    	Pin(0)、Pin(2)、Pin(4)、Pin(12~15)、Pin(27)

#### 定义TouchPad
示例：

```py
from machine import TouchPad, Pin

tp = TouchPad(Pin(12))
```

### 类函数

#### 1. TouchPad.read()
函数说明：读取touchpad的电平。若touchpad接高电平则返回1，若接GND则返回0。
<br>示例：

```py
print(tp.read())
```

#### 2. TouchPad.config(value)
函数说明：设置触摸板的标识。

	value：任意整数值
<br>示例：

```py
tp.config(1)
```