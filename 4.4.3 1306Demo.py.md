## ssd1306（OLED）

### 准备
硬件：
* FireBeetle-ESP32 × 1
* FireBeetle Covers-OLED12864 Display模块 × 1 （<a href="http://wiki.dfrobot.com.cn/index.php?title=%28SKU:DFR0507%29FireBeetle_Covers-OLED12864_Display" title="OLED" target="_blank">点击查看</a>模块详情）

软件：
* uPyCraft IDE

代码位置：
* <p>File → Examples → Display → <b>ssd1306Demo.py</b></p>
* <p>引用模块：uPy_lib → <b>ssd1306.py</b></p>

### 实验步骤
<p>1. 硬件连接，如下图</p>

<img src=images/5.66.png>

<p>2. 将 uPy_Lib 目录下的 ssd1306.py 下载到板子中，如下图</p>

<img src=images/4.20.png>

<p>3. 运行 ssd1306Demo.py，具体代码如下</p>

```py
#硬件平台：FireBeetle-ESP32

from machine import Pin, I2C
import ssd1306

i2c = I2C(scl=Pin(22), sda=Pin(21), freq=100000)
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

lcd.text("DFRobot", 0, 0)
for i in range(0, 28):
  lcd.pixel(2*i, 10, 1)

lcd.line(0, 12, 54, 12, 1)       #画一条蓝色的线，起始坐标为（0,12），（54,12）
lcd.hline(10, 32, 108, 1)        #画一条蓝色的水平线，起始点为（10,32），长度为108
lcd.vline(64, 0, 64, 1)          #画一条蓝色的垂直线，起始点为（64,0），高度为64
lcd.fill_rect(59, 27, 10, 10, 1) #画一个矩形，填充蓝色，对角点坐标为（59,27），（10，10)
lcd.rect(56, 24, 16, 16, 1)      #画一个蓝色矩形框，对角点坐标为（59,24），（16,16）
lcd.fill_rect(59, 27, 10, 10, 1)
lcd.fill_rect(88, 0, 40, 20, 1)
lcd.line(88, 0, 128, 20, 0)      #画黑色线
lcd.line(88, 20, 128, 0, 0)
lcd.show()                       #显示
```
说明：
<br>&nbsp;&nbsp;&nbsp;SSD1306目前只支持两种颜色，即蓝色（1）和黑色（0）。

### 实验效果

<img src=images/4.13.4.jpg>

### 实验扩展
&nbsp;&nbsp;&nbsp;学了上面的例子，相信你也基本掌握了这个模块，现在我们来写一个程序，当文字碰到OLED屏边缘时自动反弹，同时文字改变。有兴趣的话，下载下来看看吧。当然，你也可以根据自己的需要来显示文字和设计自己的图形，甚至可以让你的图形动起来。另外要注意lcd.show()函数很重要，用来显示刷新文字和图形喔。

**注意：**
<br>
<p>&nbsp;&nbsp;&nbsp;因为我们之前已经下载了 ssd1306.py，所以直接将下面的程序保存为.py文件运行即可。</p>

```py
#hardware platform: FireBeetle-ESP32
from machine import Pin,I2C
import ssd1306
import time
import machine
i2c = I2C(scl=Pin(22), sda=Pin(21), freq=100000)
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

x1,y1,x2,y2,x3,y3,x4,y4 = (4,4,50,64,0,0,0,0)
bufx,bufy = bytearray(300),bytearray(300)
str1,str2 = "OLED...","DFRobot"
i,k,corr = (0, 10, 3)

def getPoint(x, y):
  global i, k, x4, y4
  k = 0
  if(x<3 or x>72):
    k,x4,y4 = (1 if x<3 else 2),(3 if x<3 else 72),y
  elif(y<3 or y>60):
    k,y4,x4 = (3 if y<3 else 4),(3 if y<3 else 60),x
  if not k:
    bufx[i],bufy[i],i = x,y,i+1
  return k

def DDALine(x1, y1, x2, y2):
  if x1==x2 or y1==y2:
    x2=x1+16*corr if x1<20 else x1-16*corr
    if (y1<20 and x1<20) or (y1<20 and x1>20):
      y2 = y1+8*corr 
    elif (y1>20 and x1<20) or (y1>20 and x1>20):
      y2 = y1-8*corr
  xerr,yerr,uRow,uCol,delta_x,delta_y = 0,0,x1,y1,x2-x1,y2-y1
  if(delta_x >= 0):
    incx=1 if delta_x>0 else 0
  else:
    incx,delta_x = -1,-delta_x
  if(delta_y >= 0):
    incy=1 if delta_y>0 else 0
  else:
    incy,delta_y = -1,-delta_y
  distance=delta_x if delta_x>delta_y else delta_y
  for t in range(0, distance+2):
    if(getPoint(uRow, uCol)):
      return
    xerr += delta_x
    yerr += delta_y
    if(xerr > distance):
      xerr -= distance
      uRow += incx
    if(yerr > distance):
      yerr -= distance
      uCol += incy

def show(s):
  global i
  for j in range(0,i//2):
    lcd.fill(0)
    lcd.text(s, bufx[j*2]-3,bufy[j*2]-3)
    lcd.show()
  i=0

try:
  DDALine(x1, y1, x2, y2)
  while 1:
    if(k == 0):
      x1,y1,x3,y3,x2,y2 = x2,y2,x4,y4,2*x2-x4,2*y2-y4
      DDALine(x1,y1,x2,y2)
      x1,y1 = x3,y3
    elif(k < 3):
      show(str2)
      x1,y1,x2,y2 = x4,y4,x1,2*y4-y1
      if(abs(y1-y2)<8*corr or abs(y1-y2)>12*corr):
        y2=y1-8*corr if y1>=y2 else y1+8*corr
      DDALine(x1, y1, x2, y2)
    else:
      show(str1)
      x1,y1,x2,y2 = x4,y4,2*x4-x1,y1
      if(abs(x1-x2)>16*corr or abs(x1-x2)<8*corr):
        x2=x1-16*corr if x1>=x2 else x1+16*corr
      DDALine(x1, y1, x2, y2)
except:
  machine.reset()
```

### ssd1306.py库API说明

<br>&nbsp;&nbsp;&nbsp;\*在程序中涉及到类的继承，所谓继承就是当一个类A继承另一个类B后，类A就拥有类B的所有属性。我们将被继承的类B叫父类，将继承的类A叫子类。ssd1306库中的类SSD1306_I2C和类SSD1306_SPI就是类SSD1306的子类。

#### **类 — SSD1306**
* class SSD1306(width, height, external_vcc)

```
width：SSD1306屏幕分辨率的宽度，为128
height：SSD1306屏幕分辨率的高度，为64
external_vcc：是否外部供电
	True、False
```
    
#### **类函数**
* fill(col)
函数功能：用颜色填充屏幕。

```
col：颜色
```
* pixel(x, y, col)
函数功能：画点。

```
x, y：点坐标（x, y）
col ：颜色
```
* scroll(dx, dy)
函数功能：按照给定的向量（dx,dy）移动屏幕中的内容。

**注意：**
<br>&nbsp;&nbsp;&nbsp;这可能会在屏幕上留下以前颜色的占位面积。

```
dx：水平方向移动的距离和方向
	dx为正数时向右移动，为负数时向左移动
dy：垂直方向移动的距离和方向
	dy为正数时向下移动，为负数时向下移动
```
* text(string, x, y, col=1)
函数功能：显示字符串。

```
string：待显示的字符串
x, y  ：显示的坐标（x, y）
col   ：颜色，默认为1（蓝色）
```
* hline(x, y, w, col)
函数功能：画水平线。

```
x, y：水平线起点的坐标（x, y）
w   ：水平线的宽度
col ：颜色
```
* vline(x, y, h, col)
函数功能：画垂直线。

```
x, y：垂直线起点的坐标（x, y）
h   ：垂直线的高度
col ：颜色
```
* rect(x, y, w, h, col)
函数说明：画矩形框。

```
x, y：矩形框左上角点的坐标（x, y）
w   ：宽度
h   ：高度
col ：颜色
```
* fill_rect(x, y, w, h, col)
函数功能：画矩形，并用颜色填充。

```
x, y：矩形左上角点的坐标（x, y）
w   ：宽度
h   ：长度
col ：颜色
```
* blit(fbuf, x, y)
函数功能： 使用framebuffer在屏幕上开辟一块可编辑区域，这块区域具有fill()、 pixel()、scroll()、text()、hline()、vline()、line()、rect()、fill_rect()、 blit()函数的功能。

```
fbuf：FrameBuffer对象
x, y：FrameBuffer对象坐标（x, y）
```
示例：

```py
from machine import Pin, I2C
import framebuf
import ssd1306

i2c = I2C(scl=Pin(22), sda=Pin(21), freq=100000)
lcd = ssd1306.SSD1306_I2C(128, 64, i2c)

buffer = bytearray(56 * 8 // 8)        #开辟56*8像素空间
framebufnew = framebuf.FrameBuffer(buffer, 56, 8, framebuf.MVLSB) #创建新的framebuffer对象
framebufnew.text("DFRobot", 0, 0)      #新的framebuffer输入字符串
lcd.blit(framebufnew, 0, 17)           #新的frambuffer起始坐标
lcd.show()
```

#### **类 — SSD1306_I2C**
* class SSD1306.SSD1306(width, height, i2c, addr=0x3c, external_vcc=False)

```
width：屏幕宽度
height：屏幕高度
i2c：I2C对象
addr：I2C地址（默认地址为0x3c）
external_vcc：是否外部供电（默认否）
```

#### **类 — SSD1306_SPI**

* class SSD1306.SSD1306_SPI(width, height, spi, dc, res, cs, external_vcc=False)

```
width：屏幕宽度
height：屏幕高度
spi：SPI对象
dc：data/cmd选择信号线
res：硬件复位信号线
cs：SPI片选信号线
external_vcc：是否外部供电（默认否）
```